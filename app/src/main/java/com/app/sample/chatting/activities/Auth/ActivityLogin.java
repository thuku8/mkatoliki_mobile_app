package com.app.sample.chatting.activities.Auth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.app.sample.chatting.R;
import com.app.sample.chatting.data.Tools;
import com.app.sample.chatting.font.MyLatoTextView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by shobo on 7/14/17.
 */

public class ActivityLogin extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = ActivityLogin.class.getSimpleName();

    private View parent_view;
    MyLatoTextView createAccount;



    /* *************************************
     *              GENERAL                *
     ***************************************/
    /* TextView that is used to display information about the logged in user */

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    /* Data from the authenticated user */
    //private AuthData mAuthData;
    //set to FirebaseAuth

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        parent_view = findViewById(R.id.main_content);

        mAuth = FirebaseAuth.getInstance();

        createAccount = (MyLatoTextView) findViewById(R.id.createAccount);
        createAccount.setOnClickListener(this);


        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth == null){
                    Log.e(TAG,"No Auth Data");
                }else{
                    Log.e(TAG,"There is Auth Data");
                }
            }

//            @Override
//            public void onAuthStateChanged(AuthData authData) {
//                mAuthProgressDialog.hide();
//                setAuthenticatedUser(authData);
//            }
        };
        /* Check if the user is authenticated with Firebase already. If this is the case we can set the authenticated
         * user and hide hide any login buttons */
        mAuth.addAuthStateListener(mAuthStateListener);

        // for system bar in lollipop
        Tools.systemBarLolipop(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
//        FirebaseUser currentUser = mAuth.getCurrentUser();
        //updateUI(currentUser);



        FirebaseUser user = mAuth.getCurrentUser();

        if (user != null) {
//            // Name, email address, and profile photo Url
//            String name = user.getDisplayName();
//            String email = user.getEmail();
//            Uri photoUrl = user.getPhotoUrl();
//
//            // Check if user's email is verified
//            boolean emailVerified = user.isEmailVerified();
//
//            // The user's ID, unique to the Firebase project. Do NOT use this value to
//            // authenticate with your backend server, if you have one. Use
//            // FirebaseUser.getToken() instead.
//            String uid = user.getUid();
            Log.e(TAG,user.getUid());
        }else{
            Log.e(TAG,"currentUser.getUid()");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // if changing configurations, stop tracking firebase session.
        mAuth.removeAuthStateListener(mAuthStateListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.createAccount:
                Intent it = new Intent(ActivityLogin.this,ActivityRegister.class);
                startActivity(it);
                break;
            default:
                break;
        }
    }
}
