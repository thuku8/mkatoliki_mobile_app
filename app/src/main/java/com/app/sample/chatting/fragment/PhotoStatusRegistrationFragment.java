package com.app.sample.chatting.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.sample.chatting.R;

/**
 * Created by shobo on 7/31/17.
 */

public class PhotoStatusRegistrationFragment extends Fragment implements View.OnClickListener{

    private static final String ARG_POSITION = "position";

    private int position;
    private View rootView;

    public static PhotoStatusRegistrationFragment newInstance(int position) {
        PhotoStatusRegistrationFragment f = new PhotoStatusRegistrationFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt(ARG_POSITION);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_photo_status_signup,container, false);


//        initSpinnerMultiline();
        ViewCompat.setElevation(rootView, 50);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            default:
                break;

        }
    }
}
