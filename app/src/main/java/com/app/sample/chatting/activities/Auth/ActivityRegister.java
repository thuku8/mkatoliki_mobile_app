package com.app.sample.chatting.activities.Auth;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.app.sample.chatting.R;
import com.app.sample.chatting.data.Tools;

/**
 * Created by shobo on 7/14/17.
 */

public class ActivityRegister extends AppCompatActivity{

    private View parent_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        parent_view = findViewById(R.id.main_content);


        // for system bar in lollipop
        Tools.systemBarLolipop(this);
    }

}
