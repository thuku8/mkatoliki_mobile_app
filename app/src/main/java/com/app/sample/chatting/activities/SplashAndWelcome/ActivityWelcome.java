package com.app.sample.chatting.activities.SplashAndWelcome;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.app.sample.chatting.R;
import com.app.sample.chatting.activities.Auth.ActivityLogin;
import com.app.sample.chatting.data.Tools;
import com.app.sample.chatting.fragment.WelcomeFragment;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by shobo on 7/18/17.
 */

public class ActivityWelcome extends AppCompatActivity{

    private View parent_view;
    private MyPagerAdapter adapter;
    private ViewPager pager;
    private TextView previousButton;
    private TextView nextButton;
    private TextView skipButton;
    private TextView navigator;
    private int currentItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        parent_view = findViewById(R.id.main_content);

        currentItem = 0;

        pager = (ViewPager) findViewById(R.id.activity_welcome_pager);
        previousButton = (TextView) findViewById(R.id.activity_welcome_previous);
        nextButton = (TextView) findViewById(R.id.activity_welcome_next);
        skipButton = (TextView) findViewById(R.id.activity_welcome_skip);
        navigator = (TextView) findViewById(R.id.activity_welcome_position);

        adapter = new MyPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setCurrentItem(currentItem);

        setNavigator();

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrollStateChanged(int position) {
                // TODO Auto-generated method stub
                if (pager.getCurrentItem() == 0) {
                    previousButton.setVisibility(View.INVISIBLE);
                } else {
                    previousButton.setVisibility(View.VISIBLE);
                }
                if (pager.getCurrentItem() == (pager.getAdapter().getCount() - 1)) {
                    nextButton.setText("Got It");
                } else {
                    nextButton.setText("Next");
                }
                setNavigator();
            }

        });

        previousButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // TODO Auto-generated method stub
                if (pager.getCurrentItem() != 0) {
                    pager.setCurrentItem(pager.getCurrentItem() - 1);
                }
                setNavigator();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (pager.getCurrentItem() != (pager.getAdapter().getCount() - 1)) {
                    pager.setCurrentItem(pager.getCurrentItem() + 1);
                } else {
                    launchMainActivity();
                }
                setNavigator();
            }
        });

        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchMainActivity();
            }
        });

        // for system bar in lollipop
        Tools.systemBarLolipop(this);
    }


    public void setNavigator() {
        String navigation = "";
        for (int i = 0; i < adapter.getCount(); i++) {
            if (i == pager.getCurrentItem()) {
                navigation += getString(R.string.material_icon_point_full)
                        + "  ";
            } else {
                navigation += getString(R.string.material_icon_point_empty)
                        + "  ";
            }
        }
        navigator.setText(navigation);
    }

    public void setCurrentSlidePosition(int position) {
        this.currentItem = position;
    }

    public int getCurrentSlidePosition() {
        return this.currentItem;
    }

    //launch the main activity
    private void launchMainActivity() {

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                // go to the main activity
                Intent i = new Intent(ActivityWelcome.this, ActivityLogin.class);
                startActivity(i);
                // kill current activity
                finish();
            }
        };
        // Show splash screen for 3 seconds
        new Timer().schedule(task, 100);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }

        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                previousButton.setVisibility(View.INVISIBLE);
                return WelcomeFragment.newInstance(position);
            } else if (position == 1) {
                return WelcomeFragment.newInstance(position);
            } else {
                return WelcomeFragment.newInstance(position);
            }
        }
    }
}
