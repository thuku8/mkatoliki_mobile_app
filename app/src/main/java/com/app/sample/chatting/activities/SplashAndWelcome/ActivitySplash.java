package com.app.sample.chatting.activities.SplashAndWelcome;

import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.sample.chatting.BaseActivity;
import com.app.sample.chatting.R;
import com.app.sample.chatting.activities.Auth.ActivityPhoneAuth;
import com.app.sample.chatting.data.Tools;
import com.app.sample.chatting.services.MsgPushService;

import java.util.Timer;
import java.util.TimerTask;

public class ActivitySplash extends BaseActivity {

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        startService(new Intent(getBaseContext(), MsgPushService.class));

        bindLogos();

        // for system bar in lollipop
        Tools.systemBarLolipop(this);
    }

    private void bindLogos() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
        RelativeLayout l=(RelativeLayout) findViewById(R.id.relLayout);
        l.clearAnimation();
        l.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
        ImageView iv = (ImageView) findViewById(R.id.splashIv);
        iv.clearAnimation();
        iv.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.transalate_ex);
        anim.reset();
        TextView tv = (TextView) findViewById(R.id.splashTv);
        tv.clearAnimation();
        tv.startAnimation(anim);

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                // go to the phone auth activity
                ActivityPhoneAuth.start(activityContext);
                // kill current activity
                finish();
            }
        };
        // Show splash screen for 3 seconds
        new Timer().schedule(task, 2500);
    }
}
