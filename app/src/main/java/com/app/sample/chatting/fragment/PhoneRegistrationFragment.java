package com.app.sample.chatting.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.sample.chatting.R;
import com.app.sample.chatting.view.FloatLabeledEditText;

import fr.ganfra.materialspinner.MaterialSpinner;

/**
 * Created by shobo on 7/20/17.
 */

public class PhoneRegistrationFragment extends Fragment{

    private static final String ARG_POSITION = "position";

    private int position;
    private View rootView;

    private String[] COUNTRIESLIST = {"United States", "England", "Kenya", "Uganda", "Tanzania"};
    private static final String ERROR_MSG = "Please select your country";

    private ArrayAdapter<String> adapter;

    MaterialSpinner spinner;

    private ProgressBar mProgress;
    private int mProgressStatus = 0;
    private Handler mHandler = new Handler();

    private LinearLayout linearStep1,linearStep2;
    private ProgressBar auth_sms_progress_bar;
    private FloatLabeledEditText phoneNumber,otpCode;
    private Button sendSmsBtn,mVerifyButton;
    private ImageView editPhoneNumberBtn;
    private TextView resendSMS,editPhoneNumberTxtView;
    private String phoneNumberStr,otpCodeStr;


    private boolean shown = false;

    public static PhoneRegistrationFragment newInstance(int position) {
        PhoneRegistrationFragment f = new PhoneRegistrationFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt(ARG_POSITION);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_phone_signup,
                container, false);

//        initSpinnerMultiline();
        ViewCompat.setElevation(rootView, 50);
        return rootView;
    }

    private void initSpinnerMultiline() {
        adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, COUNTRIESLIST);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setHint("Select Country Code");
//        spinner.setError("Error");
        spinner.setPaddingSafe(0, 0, 0, 0);
    }

}
