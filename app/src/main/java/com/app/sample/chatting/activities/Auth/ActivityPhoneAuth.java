package com.app.sample.chatting.activities.Auth;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.sample.chatting.BaseActivity;
import com.app.sample.chatting.R;
import com.app.sample.chatting.data.Tools;
import com.bigbangbutton.editcodeview.EditCodeView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import fr.ganfra.materialspinner.MaterialSpinner;

/**
 * Created by shobo on 8/1/17.
 */

public class ActivityPhoneAuth extends BaseActivity implements View.OnClickListener{

    private static final String TAG = ActivityPhoneAuth.class.getSimpleName();

    public static void start(Context context) {
        Intent intent = new Intent(context, ActivityPhoneAuth.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    //WE HAVE STARTED PHONE AUTH
    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";

    private static final int STATE_INITIALIZED = 1;
    private static final int STATE_CODE_SENT = 2;
    private static final int STATE_VERIFY_FAILED = 3;
    private static final int STATE_VERIFY_SUCCESS = 4;
    private static final int STATE_SIGNIN_FAILED = 5;
    private static final int STATE_SIGNIN_SUCCESS = 6;


    private FirebaseAuth.AuthStateListener mAuthListener;

    private boolean mVerificationInProgress = false;
    private String mVerificationId = null;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    private String[] COUNTRIESLIST = {"Kenya"};
    private static final String ERROR_MSG = "Please select your country";

    private ArrayAdapter<String> adapter;

    MaterialSpinner countryCodesSpinner;

    private ProgressBar auth_sms_progress_bar;
    private int mProgressStatus = 0;
    private Handler mHandler = new Handler();

    private LinearLayout linearStep1,linearStep2;
    private EditText mPhoneNumberField;
    private TextView textViewCountryCode;
    private EditCodeView mVerificationFieldCodeView;
    private TextInputLayout mVerificationFieldErrorLayout;
    private Button mResendButton,sendSmsBtn,mVerifyButton;
    private ImageView editPhoneNumberBtn;
    private String phoneNumberStr,countryCodesStr;
    private TextView editPhoneNumberTxtView;

    private boolean shown = false;

    private ActionBar actionBar;
    private View parent_view;
    private Snackbar snack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_auth);
        parent_view = findViewById(android.R.id.content);

        // Restore instance state
        if (savedInstanceState != null) {
            onRestoreInstanceState(savedInstanceState);
        }

        initToolbar();

        countryCodesSpinner = findViewById(R.id.countryCodesSpinner);
        mPhoneNumberField = findViewById(R.id.mPhoneNumberField);
        textViewCountryCode =  findViewById(R.id.text_view_country_code);
        sendSmsBtn = findViewById(R.id.sendSmsBtn);
        linearStep1 = findViewById(R.id.linearStep1);
        linearStep2 = findViewById(R.id.linearStep2);
        editPhoneNumberTxtView = findViewById(R.id.editPhoneNumberTxtView);
        mVerificationFieldCodeView = findViewById(R.id.mVerificationFieldCodeView);
        mVerificationFieldErrorLayout = findViewById(R.id.mVerificationFieldErrorLayout);
        editPhoneNumberBtn = findViewById(R.id.editPhoneNumberBtn);
        mVerifyButton = findViewById(R.id.mVerifyButton);
        mResendButton = findViewById(R.id.mResendButton);

        //the progress bar
        auth_sms_progress_bar = findViewById(R.id.auth_sms_progress_bar);

        sendSmsBtn.setOnClickListener(this);
        editPhoneNumberBtn.setOnClickListener(this);
        mVerifyButton.setOnClickListener(this);
        mResendButton.setOnClickListener(this);

        initSpinnerMultiline();

        countryCodesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                countryCodesStr = "+254";
                textViewCountryCode.setText("+254");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                countryCodesStr = null;
            }
        });

        // Initialize phone auth callbacks
        // [START phone_auth_callbacks]
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verificaiton without
                //     user action.
                Log.e(TAG, "onVerificationCompleted:" + credential);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                // [END_EXCLUDE]

                // [START_EXCLUDE silent]
                // Update the UI and attempt sign in with the phone credential
                updateUI(STATE_VERIFY_SUCCESS, credential);
                // [END_EXCLUDE]
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.e(TAG, "onVerificationFailed", e);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                // [END_EXCLUDE]

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // [START_EXCLUDE]
                    mPhoneNumberField.setError("Invalid phone number.");
                    // [END_EXCLUDE]
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // [START_EXCLUDE]
                    Snackbar.make(findViewById(android.R.id.content), "Quota exceeded.Please try again after 6 hours",Snackbar.LENGTH_SHORT).show();
                    // [END_EXCLUDE]
                }
                // Show a message and update the UI
                // [START_EXCLUDE]
                updateUI(STATE_VERIFY_FAILED);
                // [END_EXCLUDE]
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;

                Log.e(TAG, "onCodeSent 2:" + verificationId);
                Log.e(TAG, "onCodeSent:" + mResendToken);

                // [START_EXCLUDE]
                // Update UI
                updateUI(STATE_CODE_SENT);
                // [END_EXCLUDE]
            }
        };
        // [END phone_auth_callbacks]

        // this is where we start the Auth state Listener to listen
        // for whether the user is signed in or not
        mAuthListener = firebaseAuth -> {
            // Get signedIn user
            FirebaseUser user = firebaseAuth.getCurrentUser();

            //if user is signed in, we call a helper method to save the user details to Firebase
            if (user != null) {
                // User is signed in
//                    createUserInFirebaseHelper();
                Log.e(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
            } else {
                // User is signed out
                Log.e(TAG, "onAuthStateChanged:signed_out");
            }
        };

        // for system bar in lollipop
        Tools.systemBarLolipop(this);
    }

    public void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);
        actionBar.setTitle("Phone Registration");
    }

    private void initSpinnerMultiline() {
        adapter = new ArrayAdapter<>(ActivityPhoneAuth.this, android.R.layout.simple_spinner_item, COUNTRIESLIST);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countryCodesSpinner.setAdapter(adapter);
        countryCodesSpinner.setHint("Select Country Code");
//        countryCodesSpinner.setError("Error");
        countryCodesSpinner.setPaddingSafe(0, 0, 0, 0);
    }

    // [START on_start_check_user]
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);

        // [START_EXCLUDE]
        if (mVerificationInProgress && validatePhoneNumber()) {
            startPhoneNumberVerification(mPhoneNumberField.getText().toString());
        }

        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null){
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_VERIFY_IN_PROGRESS, mVerificationInProgress);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mVerificationInProgress = savedInstanceState.getBoolean(KEY_VERIFY_IN_PROGRESS);
    }


    private void startPhoneNumberVerification(String phoneNumber) {
        if(phoneNumber.length() == 10){
            phoneNumber = phoneNumber.substring(1);
        }
        phoneNumber = countryCodesStr+phoneNumber;

        // [START start_phone_auth]
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
        // [END start_phone_auth]

        mVerificationInProgress = true;
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        // [START verify_with_code]
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        // [END verify_with_code]
        signInWithPhoneAuthCredential(credential);
    }

    // [START resend_verification]
    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }
    // [END resend_verification]

    // [START sign_in_with_phone]
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.e(TAG, "signInWithCredential:success");

                        FirebaseUser user = task.getResult().getUser();
                        // [START_EXCLUDE]
                        updateUI(STATE_SIGNIN_SUCCESS, user);
                        // [END_EXCLUDE]
                    } else {
                        // Sign in failed, display a message and update the UI
                        Log.e(TAG, "signInWithCredential:failure", task.getException());
                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            // The verification code entered was invalid
                            // [START_EXCLUDE silent]
                            mVerificationFieldCodeView.setCode("'','','','','',''");
                            mVerificationFieldErrorLayout.setError("Invalid code.");
                            // [END_EXCLUDE]
                        }
                        // [START_EXCLUDE silent]
                        // Update UI
                        updateUI(STATE_SIGNIN_FAILED);
                        // [END_EXCLUDE]
                    }
                }
            });
    }
    // [END sign_in_with_phone]

    private void signOut() {
        mAuth.signOut();
        updateUI(STATE_INITIALIZED);
    }

    private void updateUI(int uiState) {
        updateUI(uiState, mAuth.getCurrentUser(), null);
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            updateUI(STATE_SIGNIN_SUCCESS, user);
        } else {
            updateUI(STATE_INITIALIZED);
        }
    }

    private void updateUI(int uiState, FirebaseUser user) {
        updateUI(uiState, user, null);
    }

    private void updateUI(int uiState, PhoneAuthCredential cred) {
        updateUI(uiState, null, cred);
    }

    private void updateUI(int uiState, FirebaseUser user, PhoneAuthCredential cred) {
        switch (uiState) {
            case STATE_INITIALIZED:
//                // Initialized state, show only the phone number field and start button
                linearStep1.setVisibility(View.VISIBLE);
                linearStep2.setVisibility(View.GONE);
                break;
            case STATE_CODE_SENT:
                // Code sent state, show the verification field, thelinearStep1.setVisibility(View.GONE);
                linearStep2.setVisibility(View.VISIBLE);
                linearStep1.setVisibility(View.GONE);
                snack = Snackbar.make(parent_view, "Code has been sent", Snackbar.LENGTH_SHORT);
                snack.show();
                break;
            case STATE_VERIFY_FAILED:
                // Verification has failed, show all options
                linearStep2.setVisibility(View.GONE);
                linearStep1.setVisibility(View.VISIBLE);
                enableViews(sendSmsBtn, mPhoneNumberField,countryCodesSpinner);
                snack = Snackbar.make(parent_view, "Verification failed", Snackbar.LENGTH_INDEFINITE);
                snack.setAction("OK", view -> snack.dismiss());
                snack.show();
                break;
            case STATE_VERIFY_SUCCESS:
                // Verification has succeeded, proceed to firebase sign in
                linearStep1.setVisibility(View.GONE);
                linearStep2.setVisibility(View.VISIBLE);
                // Set the verification text based on the credential
                if (cred != null) {
                    if (cred.getSmsCode() != null) {
//                        mVerificationFieldErrorLayout.setError("The code is "+cred.getSmsCode());
                        mVerificationFieldCodeView.setCode(cred.getSmsCode());
//                        Toast.makeText(ActivityPhoneAuth.this, "Succeeded!!!Please enter code to sign in",Toast.LENGTH_LONG).show();
                    } else {
//                        mVerificationField.setText("Instant");
                        mVerificationFieldErrorLayout.setError("Instant!!!");
//                        Toast.makeText(ActivityPhoneAuth.this, "Instant Validation Succeeded!!!",Toast.LENGTH_LONG).show();
                    }
                }

                break;
            case STATE_SIGNIN_FAILED:
                // No-op, handled by sign-in check
                Toast.makeText(ActivityPhoneAuth.this, "Sorry,Sign in has failed!!!",Toast.LENGTH_LONG).show();
                break;
            case STATE_SIGNIN_SUCCESS:
                // Np-op, handled by sign-in check
                break;
        }

        if (user == null) {
            // Signed out//not logged in//do nothing for now
            linearStep1.setVisibility(View.VISIBLE);
            linearStep2.setVisibility(View.GONE);
        } else {
            // Signed in
            linearStep1.setVisibility(View.GONE);
            linearStep2.setVisibility(View.VISIBLE);

            ActivityPhotoStatusAuth.start(activityContext);
            finish();
            //WE CAN CHECK IF THE USER HAS A PROFILE IMAGE AND USERNAME
//            if its the first time go to the username and photo status activities
//            else if its not the first time check if the have images and usernames,if they do skip to main activity else
//            show the username and photo activity
        }
    }

    private boolean validatePhoneNumber() {

        phoneNumberStr = mPhoneNumberField.getText().toString().trim();
        if (TextUtils.isEmpty(phoneNumberStr)) {
            mPhoneNumberField.setError("Invalid phone number.");
            mPhoneNumberField.requestFocus();
            return false;
        }else{

            // lets strip the first character so we can send
            // the number to the server as 7something instead of 07something
            if(phoneNumberStr.length() == 10){
                if(!phoneNumberStr.matches("07[0-9]{8}")){
                    mPhoneNumberField.setError("Invalid phone number.");
                    mPhoneNumberField.requestFocus();
                    return false;
                }else{
                    mPhoneNumberField.setError(null);
                    return true;
                }
            }else if(phoneNumberStr.length() == 9){
                if(!phoneNumberStr.matches("7[0-9]{8}")){
                    mPhoneNumberField.setError("Invalid phone number.");
                    mPhoneNumberField.requestFocus();
                    return false;
                }else{
                    mPhoneNumberField.setError(null);
                    return true;
                }
            }else{
                mPhoneNumberField.setError("Invalid phone number.");
                mPhoneNumberField.requestFocus();
                return false;
            }
        }
    }

    private void enableViews(View... views) {
        for (View v : views) {
            v.setEnabled(true);
        }
    }

    private void disableViews(View... views) {
        for (View v : views) {
            v.setEnabled(false);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sendSmsBtn:
                phoneNumberStr = mPhoneNumberField.getText().toString();
                if (TextUtils.isEmpty(phoneNumberStr)) {
                    mPhoneNumberField.setError("Cannot be empty");
                }else if(!TextUtils.isEmpty(phoneNumberStr) && !validatePhoneNumber()){
                    mPhoneNumberField.setError("Invalid Phone number");
                }else{
                    linearStep1.setVisibility(View.GONE);
                    linearStep2.setVisibility(View.VISIBLE);
                    // start verification
                    startPhoneNumberVerification(mPhoneNumberField.getText().toString());
                }

                break;
            case R.id.mVerifyButton:
                String code = mVerificationFieldCodeView.getCode().toString();
                if (TextUtils.isEmpty(code)) {
                    Toast.makeText(ActivityPhoneAuth.this, "The verification code cannot be empty.",Toast.LENGTH_LONG).show();
                    mVerificationFieldErrorLayout.setError("Cannot be empty.");
                    return;
                }else if (!TextUtils.isEmpty(code) && code.length() != mVerificationFieldCodeView.getCodeLength()) {
                    Toast.makeText(ActivityPhoneAuth.this, "The verification code must be 6 digits",Toast.LENGTH_LONG).show();
                    mVerificationFieldErrorLayout.setError("Invalid Code.");
                    return;
                }else{
                    if(mVerificationId == null){
                        return;
                    }else{
                        verifyPhoneNumberWithCode(mVerificationId, code);
                    }
                }
                break;
            case R.id.mResendButton:
                resendVerificationCode(mPhoneNumberField.getText().toString(), mResendToken);
                break;
            case R.id.editPhoneNumberBtn:
                linearStep1.setVisibility(View.VISIBLE);
                linearStep2.setVisibility(View.GONE);
                enableViews(sendSmsBtn, mPhoneNumberField,countryCodesSpinner);
                actionBar.setTitle("Phone Registration");
                auth_sms_progress_bar.setProgress(1);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

}
