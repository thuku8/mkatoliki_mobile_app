package com.app.sample.chatting.activities.chatting;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.app.sample.chatting.BaseActivity;
import com.app.sample.chatting.R;
import com.app.sample.chatting.adapter.GroupDetailsListAdapter;
import com.app.sample.chatting.adapter.firebase.ChatsAdapter;
import com.app.sample.chatting.data.Constant;
import com.app.sample.chatting.data.Tools;
import com.app.sample.chatting.model.Group;
import com.app.sample.chatting.model.GroupDetails;
import com.app.sample.chatting.model.chat.ChatStartedDetails;
import com.app.sample.chatting.model.chat.LastChat;
import com.app.sample.chatting.model.chat.SingleUserMessages;
import com.app.sample.chatting.model.chat.UserChats;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by shobo on 2/26/18.
 */

public class ActivityOnetoOneChat extends BaseActivity {

    public static String TAG = ActivityOnetoOneChat.class.getSimpleName();

    public static String KEY_GROUP = "com.app.sample.chatting.GROUP";

    // give preparation animation activity transition
    public static void navigate(AppCompatActivity activity, View transitionImage, Group obj) {
        Intent intent = new Intent(activity, ActivityOnetoOneChat.class);
        intent.putExtra(KEY_GROUP, obj);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionImage, KEY_GROUP);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }

    private Button btn_send;
    private EditText et_content;
    public static GroupDetailsListAdapter adapter;

    private ListView listview;
    private ActionBar actionBar;
    private Group group;
    private List<GroupDetails> items = new ArrayList<>();
    private View parent_view;

    // firebase
    private DatabaseReference mChatDatabaseRef;
    private DatabaseReference mChatRef;
    private ValueEventListener mChatListener;
    /**
     * Get the last 50 chat messages.
     */
    protected static Query sChatQuery;
    private RecyclerView chatRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;

    private List<SingleUserMessages> allTask;
    private ChatsAdapter mFirebaseCustomAdapter;
    private ArrayList<SingleUserMessages> mAdapterItems;
    private ArrayList<String> mAdapterKeys;
    private final static String SAVED_ADAPTER_ITEMS = "SAVED_ADAPTER_ITEMS";
    private final static String SAVED_ADAPTER_KEYS = "SAVED_ADAPTER_KEYS";

    private Disposable isTypingDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_details);
        parent_view = findViewById(android.R.id.content);

        // animation transition
        ViewCompat.setTransitionName(parent_view, KEY_GROUP);

        // initialize conversation data
        Intent intent = getIntent();
        group = (Group) intent.getExtras().getSerializable(KEY_GROUP);
        initToolbar();
        setupFirebase();
        iniComponen();
        items = Constant.getGroupDetailsData(this);

        adapter = new GroupDetailsListAdapter(this, items);
        listview.setAdapter(adapter);
        listview.setSelectionFromTop(adapter.getCount(), 0);
        listview.requestFocus();
        registerForContextMenu(listview);

        // for system bar in lollipop
        Tools.systemBarLolipop(this);
    }

    public void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(group.getName());
        actionBar.setSubtitle(group.getMember());
    }

    public void bindView() {
        try {
            adapter.notifyDataSetChanged();
            listview.setSelectionFromTop(adapter.getCount(), 0);
        } catch (Exception e) {
        }
    }

    public void iniComponen() {
        final Random r = new Random();
        listview = findViewById(R.id.listview);
        btn_send = findViewById(R.id.btn_send);
        btn_send.setOnClickListener(view -> {
//            int index = r.nextInt(group.getFriends().size()-1);
//
//            items.add(items.size(), new GroupDetails(  0, Constant.formatTime(System.currentTimeMillis()), group.getFriends().get(0), et_content.getText().toString(), true));
//            items.add(items.size(), new GroupDetails( 0, Constant.formatTime(System.currentTimeMillis()), group.getFriends().get(index), et_content.getText().toString(), false));
//
//            et_content.setText("");
//            bindView();

            testWithFirebase(false);
//            hideKeyboard();
        });
        et_content = findViewById(R.id.text_content);
        // If EditText loses focus
        et_content.setOnFocusChangeListener((view, hasFocus) -> {
            //focus has stopped perform your desired action
            if (!hasFocus) {
                //user has focused
                typingIndicatorService(false);
            }
        });
        et_content.addTextChangedListener(contentWatcher);
        if (et_content.length() == 0) {
            btn_send.setEnabled(false);
        }
        hideKeyboard();

        isTypingDisposable = RxFirebaseDatabase.observeValueEvent(mChatDatabaseRef.child("isTyping"),dataSnapshot -> {
            // do your own mapping here
//                    return new Author();
            return (boolean) dataSnapshot.getValue();
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(isTyping -> {
                    if(isTyping){
                        actionBar.setSubtitle("you are typing...");
                    }else{
                        actionBar.setSubtitle(group.getMember());
                    }
                }, throwable -> {
                    Log.e(TAG,throwable.toString());
                    actionBar.setSubtitle(group.getMember());
                });


        // set up data
        chatRecyclerView = findViewById(R.id.chatRecyclerView);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setStackFromEnd(true);
        chatRecyclerView.setLayoutManager(mLinearLayoutManager);

        mFirebaseCustomAdapter = new ChatsAdapter(ActivityOnetoOneChat.this,sChatQuery, mAdapterItems, mAdapterKeys);
        //REGISTER DATA OBSERVER
//        registerRecyclerAdapterDataObserver(true);
        chatRecyclerView.setAdapter(mFirebaseCustomAdapter);
    }

    private void setupFirebase() {
        mChatRef = mDatabase;
        mChatDatabaseRef = mDatabaseUsers.child(Constant.FIREBASE_LOCATION_FAKE_USER_ID).child(Constant.FIREBASE_LOCATION_USER_CHAT_THREADS).child(getUid());
        mChatDatabaseRef.keepSynced(true);

        // remember to get the chat thread id from intents or firebase query
        final String userChatThreadId = "-L6aA7L1V7FKWTy_cRbE";
        sChatQuery = mDatabase.child(Constant.FIREBASE_LOCATION_ONE_TO_ONE_USER_CHAT_THREADS)
                .child(userChatThreadId).child("chat-messages")
                .limitToLast(2000);

    }

    private void testWithFirebase(boolean useTransactions) {

        if(useTransactions){
            mChatDatabaseRef.runTransaction(new Transaction.Handler() {
                @Override
                public Transaction.Result doTransaction(MutableData mutableData) {

                    UserChats userChat = mutableData.getValue(UserChats.class);

                    if(userChat == null || mutableData.getChildrenCount() < 1){
                        writeMessageToUser(false,et_content.getText().toString().trim(),userChat);
                    }else{
                        writeMessageToUser(true,et_content.getText().toString().trim(),userChat);
                    }
                    return Transaction.success(mutableData);
                }

                @Override
                public void onComplete(DatabaseError databaseError, boolean success, DataSnapshot dataSnapshot) {
                    if (databaseError != null || !success || dataSnapshot == null) {
                        Log.e(TAG,"Failed to get DataSnapshot");
                    } else {
                        Log.e(TAG,"Successfully get DataSnapshot");
                        //handle data here
                    }
                }
            });
        }else{
            // Add value event listener to the user
            ValueEventListener userChatListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    UserChats userChat = dataSnapshot.getValue(UserChats.class);
                    if(userChat == null || dataSnapshot.getChildrenCount() < 1){
                        writeMessageToUser(false,et_content.getText().toString().trim(),userChat);
                    }else{
                        writeMessageToUser(true,et_content.getText().toString().trim(),userChat);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, "loadPost:onCancelled", databaseError.toException());
                }
            };
            // you can also add addListenerForSingleValueEvent
            mChatDatabaseRef.addListenerForSingleValueEvent(userChatListener);
            // Keep copy of listener so we can remove it when app stops
            mChatListener = userChatListener;
        }


    }

    private void writeMessageToUser(boolean chatThreadExists,String messageToSend,UserChats userChat) {
        Date timeNow = new Date();
        String userChatThreadId = null;
        String newChatMessageKey = null;
        UserChats otherUserChatObj;
        UserChats meUserChatObj;

        if(!chatThreadExists){
            userChatThreadId = mChatRef.child(Constant.FIREBASE_LOCATION_ONE_TO_ONE_USER_CHAT_THREADS).push().getKey();
            newChatMessageKey = mChatRef.child(Constant.FIREBASE_LOCATION_ONE_TO_ONE_USER_CHAT_THREADS).child(userChatThreadId).push().getKey();

            ChatStartedDetails chSD = new ChatStartedDetails(timeNow.getTime(),getUid());
            // create a last chat object
            LastChat meLastChat = new LastChat(newChatMessageKey,timeNow.getTime(),true,getUid(),messageToSend,Constant.MESSAGE_TYPE_TEXT);
            LastChat otherUserLastChat = new LastChat(newChatMessageKey,timeNow.getTime(),false,getUid(),messageToSend,Constant.MESSAGE_TYPE_TEXT);
            // create a new object
            otherUserChatObj = new UserChats(chSD, otherUserLastChat, userChatThreadId,0,false,getUid());
            meUserChatObj = new UserChats(chSD, meLastChat, userChatThreadId,0,false,Constant.FIREBASE_LOCATION_FAKE_USER_ID);

        }else{
            userChatThreadId = userChat.getChatThread();
            newChatMessageKey = mChatRef.child(Constant.FIREBASE_LOCATION_ONE_TO_ONE_USER_CHAT_THREADS).child(userChatThreadId).push().getKey();
            // create a last chat object
            LastChat meLastChat = new LastChat(newChatMessageKey,timeNow.getTime(),true,getUid(),messageToSend,Constant.MESSAGE_TYPE_TEXT);
            LastChat otherUserLastChat = new LastChat(newChatMessageKey,timeNow.getTime(),false,getUid(),messageToSend,Constant.MESSAGE_TYPE_TEXT);
            // check nulls here later
            otherUserChatObj = new UserChats(userChat.details,otherUserLastChat,userChatThreadId,(userChat.getUnreadCount())+1,userChat.isGroup,userChat.otherParticipant);
            meUserChatObj = new UserChats(userChat.details,meLastChat,userChatThreadId,0,userChat.isGroup,userChat.otherParticipant);
        }

        //create message object
        // create the readBy hashmap
        Map<String, Boolean> readByMap = new HashMap<String, Boolean>() {{
            put(getUid(), true);
            put(Constant.FIREBASE_LOCATION_FAKE_USER_ID, false);
        }};
        SingleUserMessages userMessage = new SingleUserMessages(getUid(),Constant.FIREBASE_LOCATION_FAKE_USER_ID,timeNow.getTime(),
                messageToSend,Constant.MESSAGE_TYPE_TEXT,readByMap);
        Map<String, Object> userMessageValues = userMessage.toMap();

        Map<String, Object> otherUserChatValues = otherUserChatObj.toMap();
        Map<String, Object> meUserChatValues = meUserChatObj.toMap();
        Map<String, Object> childUpdates = new HashMap<>();

        //add to chat messages thread
        childUpdates.put(Constant.FIREBASE_LOCATION_ONE_TO_ONE_USER_CHAT_THREADS+"/"+userChatThreadId+"/chat-messages/"+newChatMessageKey, userMessageValues);
        //add chat threads to users recent chats
        childUpdates.put("/users/"+getUid()+"/"+Constant.FIREBASE_LOCATION_USER_CHAT_THREADS+"/"+Constant.FIREBASE_LOCATION_FAKE_USER_ID, meUserChatValues);
        childUpdates.put("/users/"+Constant.FIREBASE_LOCATION_FAKE_USER_ID+"/"+Constant.FIREBASE_LOCATION_USER_CHAT_THREADS+"/"+getUid(), otherUserChatValues);
        // Do a deep-path update
        mChatRef.updateChildren(childUpdates, (firebaseError, firebase) -> {
            if (firebaseError != null) {
                Log.e("Error updating data: ",firebaseError.getMessage());
            }
        });
    }

    private TextWatcher contentWatcher = new TextWatcher() {
        CountDownTimer timer = null;

        @Override
        public void afterTextChanged(Editable etd) {

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            String messageText = et_content.getText().toString().trim();
            if (!TextUtils.isEmpty(messageText) && messageText.length() > 0) {
                btn_send.setEnabled(true);
                typingIndicatorService(true);
                // Typed text length remains same for 3 seconds.
                if (timer != null) {
                    timer.cancel();
                }
                timer = new CountDownTimer(3000, 1000) {
                    public void onTick(long millisUntilFinished) {
                    }
                    public void onFinish() {
                        typingIndicatorService(false);
                    }
                }.start();
            } else {
                btn_send.setEnabled(false);
                typingIndicatorService(false);
            }

        }
    };

    private void typingIndicatorService(boolean typingStarted) {
        // set the typing indicator to true or false for the other user
        // he or shes chatting with
        mChatDatabaseRef = mDatabaseUsers.child(Constant.FIREBASE_LOCATION_FAKE_USER_ID).child(Constant.FIREBASE_LOCATION_USER_CHAT_THREADS).child(getUid());
        // please check if there is such a thread on activity create
        mChatDatabaseRef.child("isTyping").setValue(typingStarted);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_group_details, menu);
        return true;
    }

    /**
     * Handle click on action bar
     **/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_sample:
                Snackbar.make(parent_view, item.getTitle() + " Clicked ", Snackbar.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Remove chats value event listener
        if (mChatListener != null) {
            mChatDatabaseRef.removeEventListener(mChatListener);
        }
        // If app goes to background
        typingIndicatorService(false);
        isTypingDisposable.dispose();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mFirebaseCustomAdapter !=null){
            mFirebaseCustomAdapter.destroy();
        }
        // If app goes to background
        typingIndicatorService(false);
        isTypingDisposable.dispose();
    }

    // Saving the list of items and keys of the items on rotation
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(mFirebaseCustomAdapter !=null){
            outState.putParcelable(SAVED_ADAPTER_ITEMS, Parcels.wrap(mFirebaseCustomAdapter.getItems()));
            outState.putStringArrayList(SAVED_ADAPTER_KEYS, mFirebaseCustomAdapter.getKeys());
        }
    }

    // Restoring the item list and the keys of the items: they will be passed to the adapter
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null &&
                savedInstanceState.containsKey(SAVED_ADAPTER_ITEMS) &&
                savedInstanceState.containsKey(SAVED_ADAPTER_KEYS)) {
            mAdapterItems = Parcels.unwrap(savedInstanceState.getParcelable(SAVED_ADAPTER_ITEMS));
            mAdapterKeys = savedInstanceState.getStringArrayList(SAVED_ADAPTER_KEYS);
        } else {
            mAdapterItems = new ArrayList<SingleUserMessages>();
            mAdapterKeys = new ArrayList<String>();
        }
        Log.e("started 2",mAdapterItems.toString());
    }
}

