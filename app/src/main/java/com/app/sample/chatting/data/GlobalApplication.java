package com.app.sample.chatting.data;

import android.app.Application;
import android.content.Context;
import android.os.SystemClock;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.app.sample.chatting.R;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;

import java.util.concurrent.TimeUnit;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by shobo on 7/13/17.
 */

public class GlobalApplication extends Application {

    private static String TAG = "GlobalApplication";

    private static GlobalApplication mInstance;


    static boolean isInitialized = false;
    /*
        ****If you have faced a problem with Firebase when run application below API 19(< 4.4.2)
        ****devices due to error of Multidex. Then below solution work for me:
    */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }


    @Override
    public void onCreate() {
        super.onCreate();

        // Don't do this! This is just so cold launches take some time
        SystemClock.sleep(TimeUnit.SECONDS.toMillis(0));
        // set one font for all TextViews
//        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/MavenPro-Regular.ttf"); // font from assets: "assets/fonts/Roboto-Regular.ttf

        FirebaseApp.initializeApp(this);
        mInstance = this;//initialize Calligraphy custom fonts package
        MultiDex.install(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder().setDefaultFontPath("fonts/futura_book.ttf").setFontAttrId(R.attr.fontPath).build());

        // initialize firebase
        FirebaseDatabase mDatabase = FirebaseDatabase.getInstance();
        mDatabase.setPersistenceEnabled(true);

        // initialize firestore
        FirebaseFirestore mFirestoreDb = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build();
        mFirestoreDb.setFirestoreSettings(settings);
        // Enable Firestore logging
        FirebaseFirestore.setLoggingEnabled(true);


        try{
            if(!isInitialized){
                //FirebaseDatabase.getInstance().setPersistenceEnabled(true);
                Log.e(TAG,"No Initialized");
                isInitialized = true;
            }else {
                Log.e(TAG,"Already Initialized");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Gets the application context.
     *
     * @return the application context
     */
    public static Context getContext() {
        if (mInstance == null) {
            mInstance = new GlobalApplication();
        }
        return mInstance;
    }
}
