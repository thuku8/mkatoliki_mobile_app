package com.app.sample.chatting.model.chat;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by shobo on 2/26/18.
 */
public class ChatStartedDetails implements Serializable{
    private long creation_date;
    private String creator_entity_id;

    public ChatStartedDetails() {
    }

    public ChatStartedDetails(long creation_date, String creator_entity_id) {
        this.creation_date = creation_date;
        this.creator_entity_id = creator_entity_id;
    }

    public long getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(long creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreator_entity_id() {
        return creator_entity_id;
    }

    public void setCreator_entity_id(String creator_entity_id) {
        this.creator_entity_id = creator_entity_id;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("creation_date", creation_date);
        result.put("creator_entity_id", creator_entity_id);
        return result;
    }
}
