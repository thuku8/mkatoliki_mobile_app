package com.app.sample.chatting.activities.Auth;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.app.sample.chatting.R;
import com.app.sample.chatting.data.Tools;
import com.app.sample.chatting.fragment.PhoneRegistrationFragment;
import com.app.sample.chatting.fragment.PhotoStatusRegistrationFragment;

/**
 * Created by shobo on 7/20/17.
 */

public class ActivitySignUp extends AppCompatActivity {

    public final static String SIGN_UP_PHONE_REG_STRING = "Phone Registration";
    public final static String SIGN_UP_PHONE_VERIFICATION_STRING = "SMS Verification";
    public final static String SIGN_UP_PHONE_PHOTO_STATUS_STRING = "Photo & Status";

    private ActionBar actionBar;
    private View parent_view;


    public MyPagerAdapter adapter;
    public ViewPager pager;
//    private TextView previousButton;
//    private TextView nextButton;
//    private TextView skipButton;
//    private TextView navigator;
    private int currentItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        parent_view = findViewById(android.R.id.content);

        initToolbar();

        currentItem = 0;

        pager = (ViewPager) findViewById(R.id.activity_signup_pager);
//        previousButton = (TextView) findViewById(R.id.activity_welcome_previous);
//        nextButton = (TextView) findViewById(R.id.activity_welcome_next);
//        skipButton = (TextView) findViewById(R.id.activity_welcome_skip);
//        navigator = (TextView) findViewById(R.id.activity_welcome_position);

        adapter = new MyPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setCurrentItem(currentItem);

//        setNavigator();

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrollStateChanged(int position) {
                // TODO Auto-generated method stub

//                setNavigator();
            }

        });


        // for system bar in lollipop
        Tools.systemBarLolipop(this);
    }

    public void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
//        actionBar.setTitle("SIGN UP");
        setActionBarTitle(SIGN_UP_PHONE_REG_STRING);

        TextView skipTxt = (TextView) findViewById(R.id.skipTxt);
        skipTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("CLICKED","I WAS CLICKED");
            }
        });
    }


//    public void setNavigator() {
//        String navigation = "";
//        for (int i = 0; i < adapter.getCount(); i++) {
//            if (i == pager.getCurrentItem()) {
//                navigation += getString(R.string.material_icon_point_full)
//                        + "  ";
//            } else {
//                navigation += getString(R.string.material_icon_point_empty)
//                        + "  ";
//            }
//        }
//        navigator.setText(navigation);
//    }

    public void setCurrentSlidePosition(int position) {
        this.currentItem = position;
    }

    public int getCurrentSlidePosition() {
        return this.currentItem;
    }

    //launch the main activity
    private void launchMainActivity() {




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
//                previousButton.setVisibility(View.INVISIBLE);
                return PhoneRegistrationFragment.newInstance(position);
            } else if (position == 1) {
                return PhotoStatusRegistrationFragment.newInstance(position);
            } else {
                return PhotoStatusRegistrationFragment.newInstance(position);
            }
        }
    }

    public void setActionBarTitle(String title) {
        //changing action bar title
        if (title.equals("")) {
            actionBar.setTitle(getString(R.string.app_name));
        }else{
            actionBar.setTitle(title);
        }
    }

    public void changeFragment(int position){

        pager.setCurrentItem(position);

//        if (pager.getCurrentItem() != (pager.getAdapter().getCount() - 1)) {
//            pager.setCurrentItem(pager.getCurrentItem() + 1);
//        } else {
//            launchMainActivity();
//        }
    }
}
