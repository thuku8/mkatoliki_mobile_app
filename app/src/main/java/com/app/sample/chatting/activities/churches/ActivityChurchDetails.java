package com.app.sample.chatting.activities.churches;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.sample.chatting.ActivityChatDetails;
import com.app.sample.chatting.BaseActivity;
import com.app.sample.chatting.R;
import com.app.sample.chatting.model.Churches;

public class ActivityChurchDetails extends BaseActivity
        implements AppBarLayout.OnOffsetChangedListener{

    public static final String EXTRA_OBJCT = "com.app.sample.chatting";

    // give preparation animation activity transition
    public static void navigate(AppCompatActivity activity, View transitionImage, Churches obj) {
        Intent intent = new Intent(activity, ActivityChurchDetails.class);
        intent.putExtra(EXTRA_OBJCT, obj);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionImage, EXTRA_OBJCT);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }

    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR  = 0.9f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS     = 0.3f;
    private static final int ALPHA_ANIMATIONS_DURATION              = 200;

    private boolean mIsTheTitleVisible          = false;
    private boolean mIsTheTitleContainerVisible = true;

    private LinearLayout mTitleContainer;
    private TextView mTitle,toobarToHideTextviewTitle;
    private AppBarLayout mAppBarLayout;

    private CollapsingToolbarLayout collapsingToolbarLayout;
    private Churches friend;
    private View parent_view;
    FrameLayout mDarkTitleContainer;
    Toolbar toobarToHide;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_church_details);
        parent_view = findViewById(android.R.id.content);

        // animation transition
        ViewCompat.setTransitionName(findViewById(R.id.app_bar), EXTRA_OBJCT);

        bindActivity();

        mAppBarLayout.addOnOffsetChangedListener(this);

//        mToolbar.inflateMenu(R.menu.menu_church_details);
//        startAlphaAnimation(mTitle, 0, View.INVISIBLE);
        startAlphaAnimation(toolbar, 0, View.INVISIBLE);
//        startAlphaAnimation(mDarkTitleContainer, 0, View.INVISIBLE);
        startAlphaAnimation(toobarToHideTextviewTitle, 0, View.INVISIBLE);
//        startAlphaAnimation(mDarkTitleContainer, 0, View.GONE);
//        startAlphaAnimation(toobarToHide, 0, View.INVISIBLE);

//        mDarkTitleContainer = findViewById(R.id.dark_title_container);
        mDarkTitleContainer.setBackgroundResource(R.color.darker_darker_gray);

        friend = (Churches) getIntent().getSerializableExtra(EXTRA_OBJCT);
//        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
//        collapsingToolbarLayout.setTitle(friend.getChurchName());
//
//        ((ImageView) findViewById(R.id.image)).setImageResource(friend.getImageUrl());
//        ((Button) findViewById(R.id.bt_view_photos)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(parent_view, "View Photos Clicked ", Snackbar.LENGTH_SHORT).show();
//            }
//        });
//        ((Button) findViewById(R.id.bt_view_gallery)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(parent_view, "View Gallery Clicked ", Snackbar.LENGTH_SHORT).show();
//            }
//        });

    }

    private void bindActivity() {
        mTitle          = findViewById(R.id.main_textview_title);
        toobarToHideTextviewTitle          = findViewById(R.id.toobar_to_hide_textview_title);
//        mTitleContainer = findViewById(R.id.main_linearlayout_title);
        toolbar   = findViewById(R.id.toolbar);
        toobarToHide   = findViewById(R.id.toobar_to_hide);
        mAppBarLayout   = findViewById(R.id.app_bar);
        mDarkTitleContainer   = findViewById(R.id.dark_title_container);

        initToolbar(true);
        setToolbarTitle(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_church_details, menu);
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        try {
            return super.dispatchTouchEvent(motionEvent);
        } catch (NullPointerException e) {
            return false;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.action_send_message) {
            Intent i = new Intent(getApplicationContext(), ActivityChatDetails.class);
            i.putExtra(ActivityChatDetails.KEY_FRIEND, friend);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(offset) / (float) maxScroll;

//        handleAlphaOnTitle(percentage);
        handleToolbarTitleVisibility(percentage);
    }

    private void handleToolbarTitleVisibility(float percentage) {
        if (percentage >= PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR) {
            mDarkTitleContainer.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;

            if(!mIsTheTitleVisible) {
                 // For change `TextView` width to `WRAP_CONTENT`
                //textView.getLayoutParams().width = 200; // For change `TextView` width to 200
                startAlphaAnimation(toolbar, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleVisible = true;
            }

        } else {
            mDarkTitleContainer.getLayoutParams().height = LinearLayout.LayoutParams.WRAP_CONTENT;

            if (mIsTheTitleVisible) {
                startAlphaAnimation(toolbar, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleVisible = false;
            }
        }
    }

    private void handleAlphaOnTitle(float percentage) {
        if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
            if(mIsTheTitleContainerVisible) {
                startAlphaAnimation(mTitleContainer, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleContainerVisible = false;
            }

        } else {

            if (!mIsTheTitleContainerVisible) {
                startAlphaAnimation(mTitleContainer, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleContainerVisible = true;
            }
        }
    }

    public static void startAlphaAnimation (View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }
}