package com.app.sample.chatting.model.chat;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by shobo on 2/26/18.
 */

public class UserChats implements Serializable {
    // please note that otherParticipant is redundant and can be fetched
    // by just getting the key or the other user id

    public ChatStartedDetails details;
    public LastChat lastChat;
    public String chatThread;
    public int unreadCount = 0;
    public boolean isGroup = false;
    public boolean isTyping = false;
    public String otherParticipant;

    public UserChats() {
    }

    public UserChats(ChatStartedDetails details,LastChat lastChat,String chatThread, int unreadCount,boolean isGroup,boolean isTyping,String otherParticipant) {
        this.details = details;
        this.lastChat = lastChat;
        this.chatThread = chatThread;
        this.unreadCount = unreadCount;
        this.isGroup = isGroup;
        this.isTyping = isTyping;
        this.otherParticipant = otherParticipant;
    }

    public UserChats(ChatStartedDetails details,LastChat lastChat,String chatThread, int unreadCount,boolean isGroup,String otherParticipant) {
        this.details = details;
        this.lastChat = lastChat;
        this.chatThread = chatThread;
        this.unreadCount = unreadCount;
        this.isGroup = isGroup;
        this.otherParticipant = otherParticipant;
    }

    public UserChats(ChatStartedDetails details,String chatThread, int unreadCount,boolean isGroup,String otherParticipant) {
        this.details = details;
        this.chatThread = chatThread;
        this.unreadCount = unreadCount;
        this.isGroup = isGroup;
        this.otherParticipant = otherParticipant;
    }

    public UserChats(ChatStartedDetails details, int unreadCount,boolean isGroup,String otherParticipant) {
        this.details = details;
        this.unreadCount = unreadCount;
        this.isGroup = isGroup;
        this.otherParticipant = otherParticipant;
    }

    public UserChats(int unreadCount,boolean isGroup,String otherParticipant) {
        this.unreadCount = unreadCount;
        this.isGroup = isGroup;
        this.otherParticipant = otherParticipant;
    }

    public UserChats(int unreadCount,String otherParticipant) {
        this.unreadCount = unreadCount;
        this.otherParticipant = otherParticipant;
    }

    public UserChats(int unreadCount) {
        this.unreadCount = unreadCount;
    }

    public ChatStartedDetails getDetails() {
        return details;
    }

    public void setDetails(ChatStartedDetails details) {
        this.details = details;
    }

    public LastChat getLastChat() {
        return lastChat;
    }

    public void setLastChat(LastChat lastChat) {
        this.lastChat = lastChat;
    }

    public String getChatThread() {
        return chatThread;
    }

    public void setChatThread(String chatThread) {
        this.chatThread = chatThread;
    }

    public int getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
    }

    public boolean isGroup() {
        return isGroup;
    }

    public void setGroup(boolean group) {
        isGroup = group;
    }

    public boolean isTyping() {
        return isTyping;
    }

    public void setTyping(boolean typing) {
        isTyping = typing;
    }

    public String getOtherParticipant() {
        return otherParticipant;
    }

    public void setOtherParticipant(String otherParticipant) {
        this.otherParticipant = otherParticipant;
    }

    // [START to_map]
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("details", details);
        result.put("lastChat", lastChat);
        result.put("chatThread", chatThread);
        result.put("unreadCount", unreadCount);
        result.put("isGroup", isGroup);
        result.put("isTyping", isTyping);
        result.put("otherParticipant", otherParticipant);
        return result;
    }
    // [END to_map]

}
