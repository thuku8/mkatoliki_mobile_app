package com.app.sample.chatting.model.chat;

import android.annotation.SuppressLint;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by shobo on 3/2/18.
 */

@IgnoreExtraProperties
public class SingleUserMessages implements Serializable {
    private String id;
    private String senderId;
    private String receiverId;
    private long date;
    private String messageText;
    private int messageType;
    private String mediaUrl;
    private String mediaSize;
    private String mediaByteArrayString;
    @Exclude
    private Map<String, Boolean> readBy = new HashMap<>();

    public SingleUserMessages() {
        // Default constructor required for calls to DataSnapshot.getValue(SingleUserMessages.class)
    }

    //this handles text message only
    public SingleUserMessages(String id, String senderId, String receiverId, long date, String messageText,
                              int messageType, Map<String, Boolean> readBy) {
        this.id = id;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.date = date;
        this.messageText = messageText;
        this.messageType = messageType;
        this.readBy = readBy;
    }

    //this handles media without caption message/text
    public SingleUserMessages(String id, String senderId, String receiverId, long date, int messageType,
                              String mediaUrl, String mediaSize, String mediaByteArrayString, Map<String, Boolean> readBy) {
        this.id = id;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.date = date;
        this.messageType = messageType;
        this.mediaUrl = mediaUrl;
        this.mediaSize = mediaSize;
        this.mediaByteArrayString = mediaByteArrayString;
        this.readBy = readBy;
    }

    //this handles media with caption message/text
    public SingleUserMessages(String id, String senderId, String receiverId, long date, String messageText, int messageType,
                              String mediaUrl, String mediaSize, String mediaByteArrayString, Map<String, Boolean> readBy) {
        this.id = id;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.date = date;
        this.messageText = messageText;
        this.messageType = messageType;
        this.mediaUrl = mediaUrl;
        this.mediaSize = mediaSize;
        this.mediaByteArrayString = mediaByteArrayString;
        this.readBy = readBy;
    }

    // data that may not have ids
    // data that may not have ids
    // data that may not have ids

    //this handles text message only
    public SingleUserMessages(String senderId, String receiverId, long date, String messageText,
                              int messageType, Map<String, Boolean> readBy) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.date = date;
        this.messageText = messageText;
        this.messageType = messageType;
        this.readBy = readBy;
    }

    //this handles media without caption message/text
    public SingleUserMessages(String senderId, String receiverId, long date, int messageType,
                              String mediaUrl, String mediaSize, String mediaByteArrayString, Map<String, Boolean> readBy) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.date = date;
        this.messageType = messageType;
        this.mediaUrl = mediaUrl;
        this.mediaSize = mediaSize;
        this.mediaByteArrayString = mediaByteArrayString;
        this.readBy = readBy;
    }

    //this handles media with caption message/text
    public SingleUserMessages(String senderId, String receiverId, long date, String messageText, int messageType,
                              String mediaUrl, String mediaSize, String mediaByteArrayString, Map<String, Boolean> readBy) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.date = date;
        this.messageText = messageText;
        this.messageType = messageType;
        this.mediaUrl = mediaUrl;
        this.mediaSize = mediaSize;
        this.mediaByteArrayString = mediaByteArrayString;
        this.readBy = readBy;
    }
    // data that may not have ids
    // data that may not have ids
    // data that may not have ids

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getShortBody() {

        @SuppressLint("SimpleDateFormat")
//        DateFormat dateTimeInstance = new SimpleDateFormat("dd-MMM-yyyy");
//        return dateTimeInstance.format(new Date(date));

        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");
        return formatter.format(date);
    }

    public String getDateFormatted() {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        return formatter.format(date);
    }


    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public String getMediaSize() {
        return mediaSize;
    }

    public void setMediaSize(String mediaSize) {
        this.mediaSize = mediaSize;
    }

    public String getMediaByteArrayString() {
        return mediaByteArrayString;
    }

    public void setMediaByteArrayString(String mediaByteArrayString) {
        this.mediaByteArrayString = mediaByteArrayString;
    }

    public Map<String, Boolean> getReadBy() {
        return readBy;
    }

    public void setReadBy(Map<String, Boolean> readBy) {
        this.readBy = readBy;
    }

    // [START to_map]
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("id", id);
        result.put("senderId", senderId);
        result.put("receiverId", receiverId);
        result.put("date", date);
        result.put("messageText", messageText);
        result.put("messageType", messageType);
        result.put("mediaUrl", mediaUrl);
        result.put("mediaSize",mediaSize);
        result.put("mediaByteArrayString",mediaByteArrayString);
        result.put("readBy", readBy);
        return result;
    }
    // [END to_map]

}