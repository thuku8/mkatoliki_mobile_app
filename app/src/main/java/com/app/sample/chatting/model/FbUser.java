package com.app.sample.chatting.model;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class FbUser {

    public String id;
    public String email;
    public String username;
    public String phone;
    public String photo;

    public FbUser() {
    }

    public FbUser(String id, String phone) {
        this.id = id;
        this.phone = phone;
    }

    public FbUser(String id, String email,String username) {
        this.id = id;
        this.email = email;
        this.username = username;
    }

    public FbUser(String id, String email,String username,String phone) {
        this.id = id;
        this.email = email;
        this.username = username;
        this.phone = phone;
    }

    public FbUser(String id, String email,String username,String phone,String photo) {
        this.id = id;
        this.email = email;
        this.username = username;
        this.phone = phone;
        this.photo = photo;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("id", id);
        result.put("email", email);
        result.put("username", username);
        result.put("phone", phone);
        result.put("photo", photo);
        return result;
    }
}
