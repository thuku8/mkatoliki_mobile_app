package com.app.sample.chatting.adapter.firestore;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.sample.chatting.R;
import com.app.sample.chatting.model.Churches;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.firestore.Query;

/**
 * Created by shobo on 3/21/18.
 * RecyclerView adapter for a list of Restaurants.
 */
public class ChurchesAdapter extends FirestoreAdapter<ChurchesAdapter.ViewHolder> {
    public static ColorGenerator generator = ColorGenerator.MATERIAL;
    private Context context;

    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, Churches obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public ChurchesAdapter(Query query, Context ctx) {
        super(query);
        context = ctx;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_churches, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView itemName;
        TextView itemLocation;
        TextView itemShortHist;
        LinearLayout lyt_parent;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.item_image);
            itemName = itemView.findViewById(R.id.item_name);
            itemLocation = itemView.findViewById(R.id.item_location);
            itemShortHist = itemView.findViewById(R.id.item_short_hist);
            lyt_parent = itemView.findViewById(R.id.lyt_parent);
        }
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Churches church = getSnapshot(position).toObject(Churches.class);

        // set the id here
        church.setId(getSnapshot(position).getId());

        if(church.getImageUrl().isEmpty()){
            String letter = String.valueOf(church.getChurchName().charAt(0));
            TextDrawable drawable = TextDrawable.builder()
                    .buildRound(letter, generator.getRandomColor());
            holder.imageView.setImageDrawable(drawable);
        }else{
            Glide.with(context).load(church.getImageUrl())
                    .apply(RequestOptions.circleCropTransform()).into(holder.imageView);
        }

        holder.itemName.setText(church.getChurchName());
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            holder.itemShortHist.setText(Html.fromHtml(church.getShortHistory(),Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.itemShortHist.setText(Html.fromHtml(church.getShortHistory()));
        }

        // Here you apply the animation when the view is bound
        setAnimation(holder.itemView, position);

        holder.lyt_parent.setOnClickListener(view -> {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(view, church, position);
            }
        });
    }

    /**
     * Here is the key method to apply the animation
     */
    private int lastPosition = -1;
    private void setAnimation(View viewToAnimate, int position){
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition){
            android.view.animation.Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_in_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}

