package com.app.sample.chatting;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.multidex.MultiDex;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.app.sample.chatting.data.Constant;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

/**
 * Created by shobo on 1/8/18.
 */

public class BaseActivity extends AppCompatActivity {

    protected BaseActivity activityContext;
    public Toolbar toolbar;
    private ActionBar actionbar;

    protected AppBarLayout appBarLayout;
    protected Toolbar searchToolbar;

    public ProgressDialog progressDialog;
    private String TAG = BaseActivity.class.getSimpleName();
    protected FirebaseAuth mAuth;
    protected DatabaseReference mDatabase;
    protected DatabaseReference mDatabaseUsers;
    protected DatabaseReference mDatabaseUserChatThreads;
    protected DatabaseReference mDatabaseOneToOneChatThreads;
    protected StorageReference mStorageRef;

    public static FirebaseFirestore mFirestore;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityContext = BaseActivity.this;
        mAuth = FirebaseAuth.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference().child(getString(R.string.app_name));
        mDatabaseUsers = mDatabase.child(Constant.FIREBASE_LOCATION_USERS);
        mDatabaseUserChatThreads = mDatabase.child(Constant.FIREBASE_LOCATION_USER_CHAT_THREADS);
        mDatabaseOneToOneChatThreads = mDatabase.child(Constant.FIREBASE_LOCATION_ONE_TO_ONE_USER_CHAT_THREADS);
        mStorageRef = FirebaseStorage.getInstance().getReference().child(getString(R.string.app_name));

        // Firestore
        mFirestore = FirebaseFirestore.getInstance();
    }

//    // pass context to Calligraphy to Load Custom Font
//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    public void initToolbar(boolean isShowHome){
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionbar = getSupportActionBar();
        if(isShowHome){
            actionbar.setDisplayHomeAsUpEnabled(true);
        }else{
            actionbar.setDisplayHomeAsUpEnabled(false);
        }
        actionbar.setHomeButtonEnabled(true);
    }

    public void setToolbarTitle(String title){
        actionbar.setTitle(title);
    }

    public void showProgress(String title, String message) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        progressDialog = ProgressDialog.show(activityContext, title, message);
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    protected void showLongToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    protected void showShortToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // hide progress dialog to prevent leaks
        hideProgress();
    }

    /*
        *method to update the gallery folder or lets say refresh
     */

    protected void updateGalleryFolder(Uri imageUri) {
        sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, imageUri));
    }
}
