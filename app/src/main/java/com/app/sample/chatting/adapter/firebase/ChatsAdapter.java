package com.app.sample.chatting.adapter.firebase;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.sample.chatting.R;
import com.app.sample.chatting.model.chat.SingleUserMessages;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by shobo on 3/5/18.
 */

public class ChatsAdapter extends FirebaseRecyclerAdapter<ChatsAdapter.ViewHolder, SingleUserMessages> {
    private Context ctx;

    public ChatsAdapter(Context context,Query query, @Nullable ArrayList<SingleUserMessages> items,
                        @Nullable ArrayList<String> keys) {
        super(query, items, keys);
        this.ctx = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView sender;
        public TextView time;
        public TextView message;
        public LinearLayout lyt_parent;
        public CardView lyt_thread;
        public ImageView image_status;
        private CardView lyt_thread_formatted_date;
        private TextView date_formatted_content;

        public ViewHolder(View v) {
            super(v);
            sender = v.findViewById(R.id.sender);
            time = v.findViewById(R.id.text_time);
            message = v.findViewById(R.id.text_content);
            lyt_parent = v.findViewById(R.id.lyt_parent);
            lyt_thread = v.findViewById(R.id.lyt_thread);
            image_status = v.findViewById(R.id.image_status);
            lyt_thread_formatted_date = v.findViewById(R.id.lyt_thread_formatted_date);
            date_formatted_content = (TextView) v.findViewById(R.id.date_formatted_content);
        }
    }

    @Override
    public ChatsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_to_delete, parent, false);

        return new ViewHolder(view);
    }

    @Override public void onBindViewHolder(ChatsAdapter.ViewHolder holder, int position) {
        SingleUserMessages item = getItem(position);

        if (position > 0 && getItems().get(position - 1).getShortBody().equals(item.getShortBody())) {
            holder.lyt_thread_formatted_date.setVisibility(View.GONE);
        } else {
            holder.lyt_thread_formatted_date.setVisibility(View.VISIBLE);
            holder.date_formatted_content.setText(item.getDateFormatted());
        }
        holder.sender.setText(item.getSenderId());
        holder.message.setText(item.getMessageText());
        holder.time.setText(String.valueOf(new Date(item.getDate())));

        if(position % 2 == 0){
            holder.sender.setVisibility(View.GONE);
            holder.lyt_parent.setPadding(100, 10, 15, 10);
            holder.lyt_parent.setGravity(Gravity.RIGHT);
            holder.lyt_thread.setCardBackgroundColor(ctx.getResources().getColor(R.color.me_chat_bg));
        }else{
            holder.sender.setVisibility(View.VISIBLE);
            holder.lyt_parent.setPadding(15, 10, 100, 10);
            holder.lyt_parent.setGravity(Gravity.LEFT);
            holder.lyt_thread.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
        }

        holder.lyt_parent.setOnClickListener(v -> Log.e("Id",item.getId()));
    }

    @Override protected void itemAdded(SingleUserMessages item, String key, int position) {
        //Add the refs if you need them later
        item.setId(key);
        Log.e("ChatsAdapter", "Added a new item to the adapter.");
    }

    @Override protected void itemChanged(SingleUserMessages oldItem, SingleUserMessages newItem, String key, int position) {
        newItem.setId(key);
        Log.e("ChatsAdapter", "Changed an item.");
    }

    @Override protected void itemRemoved(SingleUserMessages item, String key, int position) {
        Log.e("ChatsAdapter", "Removed an item from the adapter.");
    }

    @Override protected void itemMoved(SingleUserMessages item, String key, int oldPosition, int newPosition) {
        Log.e("ChatsAdapter", "Moved an item.");
    }
}