package com.app.sample.chatting.activities.Auth;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.app.sample.chatting.BaseActivity;
import com.app.sample.chatting.R;
import com.app.sample.chatting.data.Constant;
import com.bumptech.glide.Glide;
import com.desmond.squarecamera.CameraActivity;
import com.soundcloud.android.crop.Crop;

import java.io.File;

public class ProfilePhotoActivity extends BaseActivity implements View.OnClickListener {

    private ImageView imageViewPreview;
    private Button cancel;
    private Button save;
    private LinearLayout savingImageLL;
    private String filePath = null;
    private boolean isUploaded = false;
    private String profilePhotoUrl = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_edit_profile_photo_activity);

        initToolbar(true);

        imageViewPreview = findViewById(R.id.imageViewPreview);
        cancel = findViewById(R.id.cancel);
        save = findViewById(R.id.save);
        savingImageLL = findViewById(R.id.savingImageLL);

        Intent intentExtras = getIntent();
        Bundle extrasBundle = intentExtras.getExtras();
        boolean galleryValue;
        boolean cameraValue;
        if (extrasBundle != null || !extrasBundle.isEmpty()) {
            profilePhotoUrl = null;
            if (intentExtras.hasExtra(Constant.EXTRA_GALLERY_ID)){
                galleryValue = extrasBundle.getBoolean(Constant.EXTRA_GALLERY_ID);
                // you can also check if this is false or true
                if(!galleryValue){
                    return;
                }
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                galleryIntent.setType("image/*");
                startActivityForResult(Intent.createChooser(galleryIntent, "Select from"), Constant.REQUEST_GALLERY);

            }else if(intentExtras.hasExtra(Constant.EXTRA_CAMERA_ID)){
                cameraValue = extrasBundle.getBoolean(Constant.EXTRA_CAMERA_ID);
                // you can also check if this is false or true
                if(!cameraValue){
                    return;
                }
                Intent startCustomCameraIntent = new Intent(ProfilePhotoActivity.this, CameraActivity.class);
                startActivityForResult(startCustomCameraIntent, Constant.REQUEST_CAMERA);
            }else if(intentExtras.hasExtra(Constant.EXTRA_VIEW_PROFILE)){
                profilePhotoUrl = extrasBundle.getString(Constant.EXTRA_VIEW_PROFILE_URL_STRING);
                setToolbarTitle("Profile Photo");
                showCancelSaveButtons(false);
                imageViewPreview.requestLayout();
                imageViewPreview.setScaleType(ImageView.ScaleType.FIT_CENTER);
                Glide.with(this).load(profilePhotoUrl).into(imageViewPreview);

            }else{
                return;
            }
        }
        cancel.setOnClickListener(this);
        save.setOnClickListener(this);
    }

    private void showCancelSaveButtons(boolean showButtons) {
        if(showButtons){
            savingImageLL.setVisibility(View.VISIBLE);
            save.setVisibility(View.VISIBLE);
            cancel.setVisibility(View.VISIBLE);
        }else{
            savingImageLL.setVisibility(View.GONE);
            save.setVisibility(View.GONE);
            cancel.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent result) {
        if (resultCode != RESULT_OK) return;
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Constant.REQUEST_GALLERY:
                    this.imageFromGallery(resultCode, result);
                    break;
                case Constant.REQUEST_CAMERA:
                    this.imageFromCamera(resultCode, result);
                    break;
                case Crop.REQUEST_CROP:
                    handleCrop(resultCode, result);
                default:
                    break;
            }
        }

        if (resultCode == Activity.RESULT_CANCELED) {
            return;
        }

        super.onActivityResult(requestCode, resultCode, result);
    }

    // image croping
    private void beginCrop(Uri source) {
        File profilePicsfolder = new File(getCacheDir(), Constant.PROFILE_IMAGES_FOLDER_COMPRESSED);
        if (!profilePicsfolder.exists()) {
            profilePicsfolder.mkdir();
        }else{
            // delete all the files stored in the cache compressed images folder
            //delete all the files in this path
            Constant.deleteImagesInCache(this,Constant.PROFILE_IMAGES_FOLDER_COMPRESSED);
        }

        String extension = MimeTypeMap.getFileExtensionFromUrl(filePath);
        if (extension == null) {
            extension = "jpg";
        }

        Uri destination = Uri.fromFile(new File(profilePicsfolder,  System.currentTimeMillis() + "." + extension));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            filePath = Crop.getOutput(result).getPath();
            updateImageView(filePath);
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Image result from camera
     * @param resultCode
     * @param data
     */
    private void imageFromCamera(int resultCode, Intent data) {
        Uri photoUri = data.getData();
        filePath = photoUri.getPath();
        updateImageView(filePath);
    }

    /**
     * Image result from gallery
     * @param resultCode
     * @param data
     */
    private void imageFromGallery(int resultCode, Intent data) {
        Uri selectedImage = data.getData();
        String [] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        filePath = cursor.getString(columnIndex);
        cursor.close();

        updateImageView(filePath);

    }


    /**
     * Update the imageView with new bitmap
     * @param filePath
     */
    private void updateImageView(String filePath) {
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
        imageViewPreview.requestLayout();
        imageViewPreview.setScaleType(ImageView.ScaleType.FIT_CENTER);
        imageViewPreview.setImageBitmap(bitmap);
        showCancelSaveButtons(true);
        setToolbarTitle("Add Photo");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.//
//        if(profilePhotoUrl != null){
            getMenuInflater().inflate(R.menu.menu_upload_profile_photo, menu);
//        }
        return true;
    }

    /**
     * Handle click on action bar
     **/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                isUploaded = false;
                onBackPressed();
                return true;
            case R.id.editProfilePhotoGallery:
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                galleryIntent.setType("image/*");
                startActivityForResult(Intent.createChooser(galleryIntent, "Select from"), Constant.REQUEST_GALLERY);
                return true;
            case R.id.editProfilePhotoCamera:
                Intent startCustomCameraIntent = new Intent(ProfilePhotoActivity.this, CameraActivity.class);
                startActivityForResult(startCustomCameraIntent, Constant.REQUEST_CAMERA);
                return true;
            case R.id.cropProfilePhoto:
                if(filePath !=null){
                    beginCrop(Uri.fromFile(new File(filePath)));
                }else{
                    Toast.makeText(activityContext, "No file was selected to edit/crop...", Toast.LENGTH_SHORT).show();
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cancel:
                isUploaded = false;
                onBackPressed();
                break;
            case R.id.save:


                break;
        }
    }

    private float calculateFileSize(File file) {
        // Get length of file in bytes
        long fileSizeInBytes = file.length();
        float fileSizeInKB = fileSizeInBytes / 1024;
        // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
//        float fileSizeInMB = fileSizeInKB / 1024;
        return fileSizeInKB;
    }

    @Override
    public void onBackPressed() {
        // put the String to pass back into an Intent and close this activity
        Intent returnIntent = new Intent();
        if(isUploaded){
            if(filePath != null){
                returnIntent.putExtra(Constant.USER_PROFILE_PHOTO_EXTRAS,filePath);
                setResult(RESULT_OK, returnIntent);
            }else{
                returnIntent.putExtra(Constant.USER_PROFILE_PHOTO_EXTRAS,"");
                setResult(RESULT_OK, returnIntent);
            }
        }else{
            returnIntent.putExtra(Constant.USER_PROFILE_PHOTO_EXTRAS,"");
            setResult(Activity.RESULT_CANCELED, returnIntent);
        }
        finish();
        super.onBackPressed();
    }
}
