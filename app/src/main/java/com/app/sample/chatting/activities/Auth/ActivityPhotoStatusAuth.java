package com.app.sample.chatting.activities.Auth;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.app.sample.chatting.ActivityMain;
import com.app.sample.chatting.BaseActivity;
import com.app.sample.chatting.R;
import com.app.sample.chatting.data.Constant;
import com.app.sample.chatting.data.Tools;
import com.app.sample.chatting.model.FbUser;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by shobo on 8/1/17.
 */

public class ActivityPhotoStatusAuth extends BaseActivity{

    private static final String TAG = ActivityPhotoStatusAuth.class.getSimpleName();

    public static void start(Context context) {
        Intent intent = new Intent(context, ActivityPhotoStatusAuth.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(intent);
    }

    private CircleImageView profPicView;
    private ImageView profPicEdit;
    private Button saveProfButton;
    private EditText profUsername;
    private DatabaseReference mUsersReference;
    private StorageReference profImageRef;
    private ValueEventListener mUserListener;
    private Bundle profPicActivityBundle;
    UploadTask uploadTask;
    private ProgressBar progressBar;
    private View parent_view;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser currentFbUser;
    private FbUser loggedInUser = new FbUser();
    private UserProfileChangeRequest userProfileUpdates;
    private int i = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_status_auth);
        parent_view = findViewById(android.R.id.content);

        initToolbar(false);
        setToolbarTitle("Profile Info");
        initViews();
        // this is for creating a fake user to be used in testing
//        createFakeUserForTesting();

        // for whether the user is signed in or not
        mAuthListener = firebaseAuth -> {
            // Get signedIn user
            currentFbUser = firebaseAuth.getCurrentUser();
            //if user is signed in, we call a helper method to save the user details to Firebase
            if (currentFbUser != null) {
                if(currentFbUser.getPhotoUrl() != null){
                    Picasso.with(this).load(currentFbUser.getPhotoUrl()).into(profPicView);
                }
                profUsername.setText(currentFbUser.getDisplayName());
                if(currentFbUser.getDisplayName() != null){
                    saveProfButton.setText("Continue");
                    checkIfUserExists();
                }
                saveProfButton.setEnabled(true);

            } else {
                // User is signed out
                Log.e(TAG, "onAuthStateChanged:signed_out");
                saveProfButton.setEnabled(false);
            }
        };

        hideKeyboard();

        // for system bar in lollipop
        Tools.systemBarLolipop(this);
    }

    private void createFakeUserForTesting() {
        FbUser fakeUser = new FbUser();
        fakeUser.setId("vA6vWOEaMhZvvWDmxLo2mRrARhm1");
        fakeUser.setEmail("rose@yahoo.com");
        fakeUser.setUsername("rosemary");
        fakeUser.setPhone("+254718002456");
        fakeUser.setPhoto("https://firebasestorage.googleapis.com/v0/b/flexxlogin.appspot.com/o/karura-events%2F1513414517193.jpg?alt=media&token=bb47d165-aa4b-4c42-9382-b84796067a18");

        mDatabaseUsers.child("vA6vWOEaMhZvvWDmxLo2mRrARhm1").updateChildren((fakeUser.toMap())).addOnCompleteListener(updateUserTask -> {
            if(updateUserTask.isSuccessful()){
                Log.e(TAG,"Fake user created...");
            }else {
                Log.e(TAG,"Error fake user not created...");
            }
        });
    }

    private void initViews() {
        profPicView = findViewById(R.id.profPicView);
        profPicEdit = findViewById(R.id.profPicEdit);
        progressBar = findViewById(R.id.progressBar);
        profPicView.setOnClickListener(v -> {
            if(currentFbUser.getPhotoUrl() != null){
                profPicActivityBundle = new Bundle();
                Intent intent = new Intent(activityContext, ProfilePhotoActivity.class);
                profPicActivityBundle.putBoolean(Constant.EXTRA_VIEW_PROFILE, true);
                profPicActivityBundle.putString(Constant.EXTRA_VIEW_PROFILE_URL_STRING, currentFbUser.getPhotoUrl().toString());
                intent.putExtras(profPicActivityBundle);
                activityContext.startActivityForResult(intent, Constant.PROFILE_PHOTO_ACTIVITY_RESULT_CODE);
            }else{
                selectImageCameraDialog(v);
            }
        });
        profPicEdit.setOnClickListener(v -> {
            selectImageCameraDialog(v);
        });
        profUsername = findViewById(R.id.profUsername);
        saveProfButton = findViewById(R.id.saveProfButton);
        saveProfButton.setOnClickListener(v->{

            String usernameString = profUsername.getText().toString().trim();

            if(usernameString.isEmpty()){
                profUsername.setFocusable(true);
                profUsername.requestFocus();
                profUsername.setError("Please enter a username");
            }else if(usernameString.length() < 3 || usernameString.length() > 8){
                profUsername.requestFocus();
                profUsername.setError("Username must be 3-8 characters");
            }else{
                profUsername.setError(null);
                //check if the username is still the same to the previous username
//                if(!usernameString.equals(currentFbUser.getDisplayName().toString())){
                    showProgress("Updating Profile","Please Wait...");

                    // you can also remove this line below
                    mUsersReference = mDatabaseUsers.child(currentFbUser.getUid());


                    userProfileUpdates = new UserProfileChangeRequest.Builder()
                            .setDisplayName(usernameString).build();
                    currentFbUser.updateProfile(userProfileUpdates).addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            //update the profile
                            if(loggedInUser != null){
                                loggedInUser.setId(currentFbUser.getUid());
                                loggedInUser.setEmail(currentFbUser.getEmail());
                                loggedInUser.setEmail(currentFbUser.getEmail());
                                loggedInUser.setUsername(usernameString);
                                loggedInUser.setPhone(currentFbUser.getPhoneNumber());
                                loggedInUser.setPhoto(currentFbUser.getPhotoUrl().toString());
//                                Map<String,Object> taskMap = new HashMap<String,Object>();
//                                taskMap.put("username", String.valueOf(usernameString));
//                                taskMap.put("photo", currentFbUser.getPhotoUrl().toString());
                                mUsersReference.updateChildren((loggedInUser.toMap())).addOnCompleteListener(updateUserTask -> {
                                    if(updateUserTask.isSuccessful()){
                                        Snackbar.make(parent_view, "Welcome to "+getString(R.string.app_name), Snackbar.LENGTH_LONG).show();
                                    }else {
                                        showShortToast("Please wait...Error updating your profile");
                                    }
                                });
                            }else{
                                //create the user
                                loggedInUser.setId(currentFbUser.getUid());
                                loggedInUser.setEmail(currentFbUser.getEmail());
                                loggedInUser.setUsername(usernameString);
                                loggedInUser.setPhone(currentFbUser.getPhoneNumber());
                                loggedInUser.setPhoto(currentFbUser.getPhotoUrl().toString());
                                mUsersReference.setValue(loggedInUser).addOnCompleteListener(setUserTask -> {
                                    if(setUserTask.isSuccessful()){
                                        Snackbar.make(parent_view, "Welcome to "+getString(R.string.app_name), Snackbar.LENGTH_LONG).show();
                                    }else {
                                        showShortToast("Please wait...Error updating your profile");
                                    }
                                });
                            }
                            hideProgress();
                            ActivityMain.start(activityContext);
                        }else{
                            hideProgress();
                            showShortToast("Please wait...Error updating your profile");
                        }
                    });

                }
//            }
        });
    }

    // load user profile from database
    private void getUserProfileFromDatabase() {
        checkIfUserExists();
    }

    private void checkIfUserExists() {

        mUsersReference = mDatabaseUsers.child(currentFbUser.getUid());

        // Add value event listener to the user
        ValueEventListener userDbListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.getChildrenCount() > 0){
                    loggedInUser = dataSnapshot.getValue(FbUser.class);
                    finish();
                    ActivityMain.start(activityContext);
                }else{
                    loggedInUser = new FbUser();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        mUsersReference.addValueEventListener(userDbListener);
        // Keep copy of listener so we can remove it when app stops
        mUserListener = userDbListener;
    }

    private void selectImageCameraDialog(View view) {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery",
                "Cancel"};


        AlertDialog.Builder builder = new AlertDialog.Builder(activityContext);
        builder.setTitle("Add Photo!");
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals("Take Photo")) {
                takePhotoFromCamera(Manifest.permission.CAMERA,true);
            } else if (items[item].equals("Choose from Gallery")) {
                takePhotoFromCamera(Manifest.permission.READ_EXTERNAL_STORAGE,false);
            } else if (items[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void takePhotoFromCamera(String requestedPermission, boolean isPhotoFromCamera) {

        new RxPermissions(activityContext).request(requestedPermission,Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .subscribe(granted -> {
                if (granted) {
                    launchProfilePhotoActivity(isPhotoFromCamera);
                } else {
                    // Oops permission denied
                    Snackbar.make(parent_view,"Permission Denied, Please allow to proceed !", Snackbar.LENGTH_LONG).show();
                }
            });

    }

    private void launchProfilePhotoActivity(boolean isPhotoFromCamera) {
        if (isPhotoFromCamera) {
            profPicActivityBundle = new Bundle();
            Intent intent = new Intent(activityContext, ProfilePhotoActivity.class);
            profPicActivityBundle.putBoolean(Constant.EXTRA_CAMERA_ID, true);
            intent.putExtras(profPicActivityBundle);
            activityContext.startActivityForResult(intent, Constant.PROFILE_PHOTO_ACTIVITY_RESULT_CODE);
        } else {
            profPicActivityBundle = new Bundle();
            Intent intent = new Intent(activityContext, ProfilePhotoActivity.class);
            profPicActivityBundle.putBoolean(Constant.EXTRA_GALLERY_ID, true);
            intent.putExtras(profPicActivityBundle);
            activityContext.startActivityForResult(intent, Constant.PROFILE_PHOTO_ACTIVITY_RESULT_CODE);
        }
    }

    // This method is called when the profilephoto activity finishes
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check that it is the ProfilePhotoActivity with an OK result
        if (requestCode == Constant.PROFILE_PHOTO_ACTIVITY_RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                // get String data from Intent
                String filePath = data.getStringExtra(Constant.USER_PROFILE_PHOTO_EXTRAS);
                if(filePath == null || filePath.equals("null") || filePath.equals("")){
                    showLongToast("No file added for upload...");
                }else{
                    uploadProfileImage(filePath);
                }

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                Log.e(TAG,"RESULT_CANCELED");
            }
        }
    }

    private void uploadProfileImage(String filePath) {
        File fileToUpload = new File(filePath);
        if(fileToUpload ==null){
            return;
        }

        progressBar.setVisibility(View.VISIBLE);
        Uri selectedImage = Uri.fromFile(fileToUpload);

        String extension = MimeTypeMap.getFileExtensionFromUrl(filePath);
        if (extension == null) {
            extension = "jpg";
        }

        //create reference to images folder and assing a name to the file that will be uploaded
//        profImageRef = mStorageRef.child(selectedImage.getLastPathSegment());
        profImageRef = mStorageRef.child(Constant.FIREBASE_STORAGE_PROFILE_PICS).child(currentFbUser.getUid()).child("profile_pic."+extension);
        //starting upload
        uploadTask = profImageRef.putFile(selectedImage);
        // Observe state change events such as progress, pause, and resume
        uploadTask.addOnProgressListener(taskSnapshot -> {
            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
            progressBar.incrementProgressBy((int) progress);
        });
        // Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener(exception -> {
            showShortToast("Error in uploading!");
            progressBar.setVisibility(View.GONE);
        }).addOnSuccessListener(taskSnapshot -> {
            Uri downloadUrl = taskSnapshot.getDownloadUrl();
            // updating profile
            showProgress("Updating Profile","Please Wait...");

            progressBar.setProgress(100);
            Picasso.with(this).load(downloadUrl).into(profPicView);

            userProfileUpdates = new UserProfileChangeRequest.Builder()
                    .setPhotoUri(downloadUrl).build();
            currentFbUser.updateProfile(userProfileUpdates).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Map<String,Object> taskMap = new HashMap<String,Object>();
                    taskMap.put("photo", downloadUrl.toString());
//                    taskMap.put("photo", currentFbUser.getPhotoUrl().toString());
                    mUsersReference.updateChildren(taskMap).addOnCompleteListener(updateUserTask -> {
                        if(updateUserTask.isSuccessful()){
                            Snackbar.make(parent_view, "Profile Photo added", Snackbar.LENGTH_LONG).show();
                        }
                        Constant.deleteImagesInCache(this,Constant.PROFILE_IMAGES_FOLDER);
                    });
                    hideProgress();

                }else{
                    hideProgress();
                    showShortToast("Please wait...Error updating profile photo.");
                }
            });
            // method below is instant but maybe we cleared cache????
//            Picasso.with(this).load(selectedImage).into(profPicView);
            // TODO::
            // If its a camera image please remember to save it and clear from cache

            // TODO::
            // delete created image from path

            // 2 seconds wait and hide progress
            new CountDownTimer(100 * 2, 100) {
                public void onTick(long millisUntilFinished) {
                    i++;
                }

                public void onFinish() {
                    i = -1;
                    progressBar.setVisibility(View.GONE);

                }
            }.start();

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    /**
     * Handle click on action bar
     **/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // on_start_check_user
    @Override
    public void onStart() {
        super.onStart();
        currentFbUser = mAuth.getCurrentUser();
        mAuth.addAuthStateListener(mAuthListener);

        if(currentFbUser!=null){
            getUserProfileFromDatabase();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null){
            mAuth.removeAuthStateListener(mAuthListener);
        }
        // Remove users value event listener
        if (mUserListener != null) {
            mUsersReference.removeEventListener(mUserListener);
        }
    }

    @Override
    protected void onResume() {
        currentFbUser = mAuth.getCurrentUser();
        if (currentFbUser != null) {
            getUserProfileFromDatabase();
        }
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
}
