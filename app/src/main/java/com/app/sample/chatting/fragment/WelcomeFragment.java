package com.app.sample.chatting.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.sample.chatting.R;

/**
 * Created by shobo on 7/19/17.
 */

public class WelcomeFragment extends Fragment {

    private static final String ARG_POSITION = "position";

    private int position;
    private LinearLayout layout;
    private TextView icon,welcome_title,welcome_text;
    private ImageView image;

    public static WelcomeFragment newInstance(int position) {
        WelcomeFragment f = new WelcomeFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt(ARG_POSITION);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_welcome,
                container, false);
        layout = (LinearLayout) rootView.findViewById(R.id.fragment_welcome_layout);
        icon = (TextView) rootView.findViewById(R.id.fragment_welcome_icon);
        image = (ImageView) rootView.findViewById(R.id.fragment_welcome_image);
        welcome_title = (TextView) rootView.findViewById(R.id.fragment_welcome_title);
        welcome_text = (TextView) rootView.findViewById(R.id.fragment_welcome_text);

        if (position == 0) {
            layout.setBackgroundColor(getResources().getColor(
                    R.color.colorPrimary));
            layout.invalidate();
//            icon.setText(R.string.material_icon_cloud_univ_first);
            icon.setVisibility(View.GONE);
            image.setVisibility(View.VISIBLE);
            image.setImageDrawable(getResources().getDrawable(R.drawable.ic_logo_white));
            welcome_title.setText(getResources().getString(R.string.app_name));
            welcome_text.setText(getResources().getString(R.string.app_welcome_screen_one_text));

        } else if (position == 1) {
            layout.setBackgroundColor(getResources().getColor(
                    R.color.material_purple_300));
            layout.invalidate();
//            icon.setText(R.string.material_icon_cloud_univ_first);
            icon.setVisibility(View.VISIBLE);
            image.setVisibility(View.GONE);
            icon.setText(R.string.material_icon_home_variant);
//            image.setImageDrawable(getResources().getDrawable(R.drawable.ic_logo_white));
            welcome_title.setText(getResources().getString(R.string.app_welcome_screen_two_title));
            welcome_text.setText(getResources().getString(R.string.app_welcome_screen_two_text));
        }  else if (position == 2) {
            layout.setBackgroundColor(getResources().getColor(
                    R.color.material_purple_500));
            layout.invalidate();
//            icon.setText(R.string.material_icon_cloud_univ_first);
            icon.setVisibility(View.GONE);
            image.setVisibility(View.VISIBLE);
//            icon.setText(R.string.material_icon_home_variant);
            image.setImageDrawable(getResources().getDrawable(R.drawable.church));
            welcome_title.setText(getResources().getString(R.string.app_welcome_screen_three_title));
            welcome_text.setText(getResources().getString(R.string.app_welcome_screen_three_text));
        }  else if (position == 3) {
            layout.setBackgroundColor(getResources().getColor(
                    R.color.material_purple_700));
            layout.invalidate();
//            icon.setText(R.string.material_icon_cloud_univ_first);
            icon.setVisibility(View.VISIBLE);
            image.setVisibility(View.GONE);
            icon.setText(R.string.material_icon_account_multiple);
//            image.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_group));
            welcome_title.setText(getResources().getString(R.string.app_welcome_screen_four_title));
            welcome_text.setText(getResources().getString(R.string.app_welcome_screen_four_text));
        } else {
            layout.setBackgroundColor(getResources().getColor(
                    R.color.material_purple_900));
            layout.invalidate();
//            icon.setText(R.string.material_icon_cloud_univ_first);
            icon.setVisibility(View.GONE);
            image.setVisibility(View.VISIBLE);
//            icon.setText(R.string.material_icon_home_variant);
            image.setImageDrawable(getResources().getDrawable(R.drawable.forum));
            welcome_title.setText(getResources().getString(R.string.app_welcome_screen_five_title));
            welcome_text.setText(getResources().getString(R.string.app_welcome_screen_five_text));
        }

        ViewCompat.setElevation(rootView, 50);
        return rootView;
    }
}
