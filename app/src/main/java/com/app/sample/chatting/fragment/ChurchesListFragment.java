package com.app.sample.chatting.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.app.sample.chatting.ActivityMain;
import com.app.sample.chatting.R;
import com.app.sample.chatting.activities.churches.ActivityChurchDetails;
import com.app.sample.chatting.adapter.firestore.ChurchesAdapter;
import com.app.sample.chatting.data.Constant;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;

import static com.app.sample.chatting.BaseActivity.mFirestore;

/**
 * Created by shobo on 3/21/18.
 */

public class ChurchesListFragment extends Fragment {

    private RecyclerView recyclerView;
//    public FriendsListAdapter mAdapter;
    private ProgressBar progressBar;
    View view;

    private Query mQuery;
    private ChurchesAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_friends, container, false);

        // Get ${LIMIT} restaurants
        mQuery = mFirestore.collection(Constant.FIRESTORE_CHURCHES)
                .orderBy("churchName", Query.Direction.DESCENDING);
//                .limit();

        recyclerView = view.findViewById(R.id.recyclerView);
        progressBar  = view.findViewById(R.id.progressBar);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);
        mAdapter = new ChurchesAdapter(mQuery, getActivity()) {
            @Override
            protected void onDataChanged() {
                // Show/hide content if the query returns empty.
                if (getItemCount() == 0) {
//                    mRestaurantsRecycler.setVisibility(View.GONE);
//                    mEmptyView.setVisibility(View.VISIBLE);
                    Log.e("Empty","No Churches");
                }
            }

            @Override
            protected void onError(FirebaseFirestoreException e) {
                // Show a snackbar on errors
                Snackbar.make(getActivity().findViewById(android.R.id.content),
                        "Error: check logs for info.", Snackbar.LENGTH_LONG).show();
            }
        };
        recyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener((v, obj, position) -> {
            Log.e("Clicked",obj.toString());
            Log.e("Clicked Id",obj.getId());


            ActivityChurchDetails.navigate((ActivityMain)getActivity(), v.findViewById(R.id.item_image), obj);
        });

        return view;
    }

    public void onRefreshLoading(){
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
        // add your code here which executes when the Fragment gets visible.
        // Start listening for Firestore updates
        if (mAdapter != null) {
            mAdapter.startListening();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // add your code here which executes when the Fragment is visible and intractable.
    }

    @Override
    public void onStop() {
        super.onStop();
        // add your code here which executes Fragment going to be stopped.
        if (mAdapter != null) {
            mAdapter.stopListening();
        }
    }
}