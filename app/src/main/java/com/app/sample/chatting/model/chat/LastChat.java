package com.app.sample.chatting.model.chat;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by shobo on 3/1/18.
 */

@IgnoreExtraProperties
public class LastChat implements Serializable {

    private String id;
    private long date;
    private boolean read = false;
    private String senderId;
    private String lastMessage;
    private int lastMessageType;
//    @Exclude
//    public Map<String, Boolean> readBy = new HashMap<>();

    public LastChat() {
        // Default constructor required for calls to DataSnapshot.getValue(LastChat.class)
    }

    public LastChat(String id, long date, boolean read, String senderId, String lastMessage, int lastMessageType) {
        this.id = id;
        this.date = date;
        this.read = read;
        this.senderId = senderId;
        this.lastMessage = lastMessage;
        this.lastMessageType = lastMessageType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public int getLastMessageType() {
        return lastMessageType;
    }

    public void setLastMessageType(int lastMessageType) {
        this.lastMessageType = lastMessageType;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("id", id);
        result.put("date", date);
        result.put("read", read);
        result.put("senderId", senderId);
        result.put("lastMessage", lastMessage);
        result.put("lastMessageType", lastMessageType);
        return result;
    }

}