package com.app.sample.chatting.model;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by shobo on 3/21/18.
 */

@IgnoreExtraProperties
public class Churches  implements Serializable {


    public String id;
    public String churchName;
    public String imageUrl;
    public long dateCreated;
    public long dateStarted;
    public String churchHistory;
    public String county;


    public Churches() {

    }

    public Churches(String churchName,String imageUrl) {
        this.churchName = churchName;
        this.imageUrl = imageUrl;
    }

    public Churches(String churchName,String imageUrl,String churchHistory) {
        this.churchName = churchName;
        this.imageUrl = imageUrl;
        this.churchHistory = churchHistory;
    }

    public Churches(String churchName,String imageUrl,long dateStarted,String churchHistory) {
        this.churchName = churchName;
        this.imageUrl = imageUrl;
        this.dateStarted = dateStarted;
        this.churchHistory = churchHistory;
    }

    public Churches(String churchName,String imageUrl,long dateCreated,long dateStarted,String churchHistory) {
        this.churchName = churchName;
        this.imageUrl = imageUrl;
        this.dateCreated = dateCreated;
        this.dateStarted = dateStarted;
        this.churchHistory = churchHistory;
    }

    public Churches(String churchName,String imageUrl,long dateCreated,long dateStarted,String churchHistory,String county) {
        this.churchName = churchName;
        this.imageUrl = imageUrl;
        this.dateCreated = dateCreated;
        this.dateStarted = dateStarted;
        this.churchHistory = churchHistory;
        this.county = county;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChurchName() {
        return churchName;
    }

    public void setChurchName(String churchName) {
        this.churchName = churchName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public long getDateStarted() {
        return dateStarted;
    }

    public void setDateStarted(long dateStarted) {
        this.dateStarted = dateStarted;
    }

    public String getChurchHistory() {
        return churchHistory;
    }

    public void setChurchHistory(String churchHistory) {
        this.churchHistory = churchHistory;
    }

    public String getShortHistory() {
        if(churchHistory.length()>120){
            return churchHistory.substring(0, 100)+"...";
        }else{
            return churchHistory;
        }
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
//        result.put("id", id);
        result.put("churchName", churchName);
        result.put("imageUrl", imageUrl);
        result.put("dateCreated", dateCreated);
        result.put("dateStarted", dateStarted);
        result.put("churchHistory", churchHistory);
        result.put("county", county);
        return result;
    }


}

